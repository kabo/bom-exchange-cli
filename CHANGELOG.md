# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.2](https://gitlab.com/kabo/bom-exchange-cli/compare/v0.0.1...v0.0.2) (2022-05-28)


### Features

* auto-detects content-type ([ab2353d](https://gitlab.com/kabo/bom-exchange-cli/commit/ab2353d5228dc23581822926b1579837f1b785c7))

### 0.0.1 (2022-05-27)
