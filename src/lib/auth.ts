import { pipe } from 'fp-ts/lib/function'
import * as TE from 'fp-ts/lib/TaskEither'
import { BaseOpts, MyError } from '../types'

export const processAuth = (tokenGetter: (x: string) => TE.TaskEither<MyError, string>, token?: string) =>
  <T extends BaseOpts>(opts: T): TE.TaskEither<MyError, T> =>
    !opts.auth && token
      ? pipe(tokenGetter(token), TE.map((auth) => ({ ...opts, auth })))
      : TE.right(opts)
