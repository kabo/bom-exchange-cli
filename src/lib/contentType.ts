import { pipe } from 'fp-ts/lib/function'
import type { SubmitOpts, SubmitOptsMaybeType } from '../types'

const detectContentType = (file: string): string =>
  pipe(
    file,
    JSON.parse,
    ({ specVersion }) => `application/vnd.cyclonedx+json; version=${specVersion}`,
  )

export const addContentType = (opts: SubmitOptsMaybeType): SubmitOpts =>
  opts.type
    ? opts as SubmitOpts
    : { ...opts, type: detectContentType(opts.file) }
