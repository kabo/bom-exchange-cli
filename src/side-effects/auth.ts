import { CognitoIdentityServiceProvider } from 'aws-sdk'
import { pipe } from 'fp-ts/lib/function'
import * as TE from 'fp-ts/lib/TaskEither'
import * as RNEA from 'fp-ts/lib/ReadonlyNonEmptyArray'
import { split } from 'fp-ts/lib/string'
import { MyError } from '../types'

export const getToken = (token: string) => pipe(
  token,
  split(':'),
  TE.tryCatchK<MyError, [ RNEA.ReadonlyNonEmptyArray<string> ], string>(
    ([ ClientId, region, REFRESH_TOKEN, DEVICE_KEY ]) =>
      new CognitoIdentityServiceProvider({ region })
        .initiateAuth({
          AuthFlow: 'REFRESH_TOKEN_AUTH',
          ClientId,
          AuthParameters: { REFRESH_TOKEN, DEVICE_KEY },
        }).promise()
        .then(({ AuthenticationResult }) => `${AuthenticationResult!.TokenType} ${AuthenticationResult!.AccessToken}`),
    String)
)
