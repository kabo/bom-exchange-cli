/* global globalThis */
import fetch from 'cross-fetch'
import * as TE from 'fp-ts/lib/TaskEither'
import { fetchText } from 'fp-fetch'
import type { MyError, SubmitOpts, GetOpts } from '../types'

// eslint-disable-next-line fp/no-mutation
globalThis.fetch = fetch

export const submitBom = ({ url, type, file, auth }: SubmitOpts): TE.TaskEither<MyError, any> =>
  fetchText(url, {
    method: 'POST',
    body: file,
    headers: {
      'Content-Type': type,
      ...(auth && { Authorization: auth }),
    },
  })

export const getBom = ({ url, identifier, type, auth }: GetOpts): TE.TaskEither<MyError, string> =>
  fetchText(`${url}?bomIdentifier=${identifier}`, {
    method: 'GET',
    headers: {
      'Accept': type,
      ...(auth && { Authorization: auth }),
    },
  })
