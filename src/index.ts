import { identity, pipe } from 'fp-ts/lib/function'
import * as E from 'fp-ts/lib/Either'
import * as TE from 'fp-ts/lib/TaskEither'
import { getBom, submitBom } from './side-effects/api'
import { getToken } from './side-effects/auth'
import { processAuth } from './lib/auth'
import { addContentType } from './lib/contentType'
import type { GetOpts, MyError, SubmitOptsMaybeType } from './types'

const { BOM_EXCHANGE_AUTH } = process.env

export const bomExchangeSubmitTE = (opts: SubmitOptsMaybeType): TE.TaskEither<MyError, any> => pipe(
  opts,
  addContentType,
  processAuth(getToken, BOM_EXCHANGE_AUTH),
  TE.chain(submitBom)
)

export const bomExchangeSubmit = (opts: SubmitOptsMaybeType): Promise<any> =>
  bomExchangeSubmitTE(opts)()
    .then(E.matchW(Promise.reject, identity))

export const bomExchangeGetTE = (opts: GetOpts): TE.TaskEither<MyError, any> => pipe(
  opts,
  processAuth(getToken, BOM_EXCHANGE_AUTH),
  TE.chain(getBom)
)

export const bomExchangeGet = (opts: GetOpts): Promise<any> =>
  bomExchangeGetTE(opts)()
    .then(E.matchW(Promise.reject, identity))
