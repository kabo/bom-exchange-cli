import { FetchError } from 'fp-fetch'

export interface BaseOpts {
  readonly url: string
  readonly auth?: string
}
export interface SubmitOptsBase extends BaseOpts {
  readonly file: string
}
export interface SubmitOptsMaybeType extends SubmitOptsBase {
  readonly type?: string
}
export interface SubmitOpts extends SubmitOptsBase {
  readonly type: string
}
export interface GetOpts extends BaseOpts {
  readonly identifier: string
  readonly type: string
}

export type MyError = string | FetchError<unknown>
