import { readFileSync } from 'fs'
import yargs from 'yargs'
import { hideBin } from 'yargs/helpers'
import { bomExchangeGet, bomExchangeSubmit } from './index'
import { GetOpts, SubmitOptsMaybeType } from './types'

const argv = yargs(hideBin(process.argv))
  .option('a', {
    alias: 'auth',
    type: 'string',
    describe: 'Authorization',
  })
  .option('u', {
    alias: 'url',
    type: 'string',
    describe: 'URL to interact with, BOM Exchange API endpoint',
    demandOption: true,
  })
  .command('get', 'Get BOM', {
    i: {
      alias: 'identifier',
      type: 'string',
      describe: 'Identifier to get, CDX URN or BOM serial number UUID URN',
      demandOption: true,
    },
    t: {
      alias: 'type',
      type: 'string',
      describe: 'BOM file content-type',
      default: 'application/vnd.cyclonedx+json; version=1.3',
    },
  })
  .command('submit', 'Submit BOM', {
    f: {
      alias: 'file',
      type: 'string',
      describe: 'Path to BOM file to submit',
      demandOption: true,
    },
    t: {
      alias: 'type',
      type: 'string',
      describe: 'BOM file content-type',
    },
  })
  .demandCommand()
  .help()
  .parseSync()

// TODO: detect content-type/accept

const p = argv._[ 0 ] === 'submit'
  ? bomExchangeSubmit({ ...argv, file: readFileSync(argv.file as string, { encoding: 'utf-8' }) } as unknown as SubmitOptsMaybeType)
  : bomExchangeGet(argv as unknown as GetOpts)
    .then(console.log)

// eslint-disable-next-line fp/no-unused-expression
p
  .catch((err: any) => { // eslint-disable-line better/explicit-return, fp/no-nil
    console.error(err) // eslint-disable-line fp/no-unused-expression
    process.exitCode = 1 // eslint-disable-line fp/no-mutation
  })
