import * as TE from 'fp-ts/lib/TaskEither'
import { BaseOpts } from '../../src/types'
import { processAuth } from '../../src/lib/auth'

describe('lib/auth', () => {
  describe('processAuth', () => {
    it('handles undefined', () => {
      expect.assertions(2)
      const getToken: any = jest.fn()
      return processAuth(getToken, undefined)({} as BaseOpts)()
        .then((result) => {
          expect(result).toEqualRight({})
          expect(getToken).not.toHaveBeenCalled()
        })
    })
    it('handles auth exists', () => {
      expect.assertions(2)
      const getToken: any = jest.fn()
      return processAuth(getToken, 'my-token')({ auth: 'some-auth' } as BaseOpts)()
        .then((result) => {
          expect(result).toEqualRight({ auth: 'some-auth' })
          expect(getToken).not.toHaveBeenCalled()
        })
    })
    it('handles calling tokenGetter', () => {
      expect.assertions(2)
      const getToken = jest.fn((_) => TE.right('Bearer some-auth'))
      return processAuth(getToken, 'my-token')({} as BaseOpts)()
        .then((result) => {
          expect(result).toEqualRight({ auth: 'Bearer some-auth' })
          expect(getToken).toHaveBeenCalledWith('my-token')
        })
    })
  })
})
