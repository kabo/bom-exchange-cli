import { addContentType } from '../../src/lib/contentType'

describe('lib/contentType', () => {
  describe('addContentType', () => {
    it('works', () => {
      const file = JSON.stringify({ specVersion: '1.3' })
      expect(addContentType({ file, url: 'my-url' })).toEqual({
        file,
        url: 'my-url',
        type: 'application/vnd.cyclonedx+json; version=1.3',
      })
    })
    it('honors manually set content type', () => {
      const file = JSON.stringify({ specVersion: '1.3' })
      expect(addContentType({ file, type: 'my-type', url: 'my-url' })).toEqual({
        file,
        url: 'my-url',
        type: 'my-type',
      })
    })
  })
})
