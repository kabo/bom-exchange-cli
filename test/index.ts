import { bomExchangeGet, bomExchangeSubmit } from '../src/index'

describe('index', () => {
  describe('bomExchangeSubmit', () => {
    it('is a function', () => {
      expect(typeof bomExchangeSubmit).toBe('function')
    })
  })
  describe('bomExchangeGet', () => {
    it('is a function', () => {
      expect(typeof bomExchangeGet).toBe('function')
    })
  })
})
